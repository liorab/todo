<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'roni',
            'email' => 'roni7777@roni.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role'=>'employee',
            ],
        [
            'name' => 'tal',
            'email' => 'tal555@tal555.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role'=>'employee',
        ],
        ]);
    }
}
